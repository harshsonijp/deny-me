APP=insecure-programming

all: build

build:
	kubectl run deny-me --image=nginx --labels app=deny-me --expose --port 80
	kubectl apply -f policy.yaml

run:
	kubectl run --rm -i --image=alpine:3.7 client-$RANDOM -- wget -q0- http://deny-me

clean:
	kubectl delete deploy deny-me
	kubectl delete service deny-me
	kubectl delete networkpolicy deny-all

.PHONY: all clean
